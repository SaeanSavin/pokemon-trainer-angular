# NgPokemonTrainer

This project is the third and final assignment of the frontend section of the Accelerate programme at Noroff. NgPokemon trainer allows the user to log in and browse pokemon! When they find a pokemon they like, the user can catch it by clicking the pokeball, displaying the selected pokemon in the users profile.

Go have fun and catch ém all!

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
