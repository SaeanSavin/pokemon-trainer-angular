export const environment = {
  production: true,
  apiUsers: "https://noroff-assignment-2.herokuapp.com/trainers",
  apiCatalogue: "https://pokeapi.co/api/v2/"
};
