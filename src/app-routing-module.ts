import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './app/guards/auth.guard';
import { LandingPage } from './app/pages/landing/landing.page';
import { PokemonCataloguePage } from './app/pages/pokemon-catalogue/pokemon-catalogue.page';
import { RegisterPage } from './app/pages/register/register.page';
import { TrainerPage } from './app/pages/trainer/trainer.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LandingPage,
  },
  {
    path: 'register',
    component: RegisterPage,
  },
  {
    path: 'catalogue',
    component: PokemonCataloguePage,
    canActivate: [AuthGuard],
  },
  {
    path: 'profile',
    component: TrainerPage,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], // Import a module
  exports: [RouterModule], // Expose module and it's features
})
export class AppRoutingModule {}
