import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _user: User | undefined;

  public get user(): User | undefined {
    return this._user;
  }

  public set user(user: User | undefined) {
    StorageUtil.storageSave(StorageKeys.User, user);
    this._user = user;
  }

  constructor() {
    this._user = StorageUtil.storageRead<User>(StorageKeys.User);
  }

  public inColletion(pokemonName: string): boolean {
    if (this._user) {
      for (const pokemon of this._user?.pokemon) {
        if (pokemon.name === pokemonName) {
          return true;
        }
      }
      return false;
    }
    return false;
  }
}
