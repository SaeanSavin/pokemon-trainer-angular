import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiUsers } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private readonly http: HttpClient) {}

  public login(username: string): Observable<User> {
    return this.checkUsername(username).pipe(
      switchMap((user: User | undefined) => {
        if (user === undefined) {
          throw new Error('User not found!');
        }
        return of(user);
      })
    );
  }

  private checkUsername(username: string): Observable<User | undefined> {
    return this.http
      .get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(map((response: User[]) => response.pop()));
  }
}
