import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  constructor(private readonly http: HttpClient) {}

  public register(username: string): Observable<User> {
    if (username.length > 0) {
      return this.checkUsername(username).pipe(
        switchMap((user: User | undefined) => {
          if (user !== undefined) {
            throw new Error('Username already exists');
          }
          const createUser = {
            username,
            pokemon: [],
          };

          const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-api-key': apiKey,
          });

          return this.http.post<User>(apiUsers, createUser, {
            headers,
          });
        })
      );
    }
    throw new Error('Enter a username!');
  }

  private checkUsername(username: string): Observable<User | undefined> {
    return this.http
      .get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(map((response: User[]) => response.pop()));
  }
}
