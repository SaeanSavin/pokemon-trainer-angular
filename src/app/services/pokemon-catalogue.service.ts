import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.util';

const { apiCatalogue } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueService {
  private _pokemons: Pokemon[] = [];
  private _error: string = '';
  private _loading: boolean = false;
  private _pageLimit = 20;
  private _offset = 0;

  private _pokemonCache?: PokemonResponse = {
    result: [],
  };

  public get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  public get error(): string {
    return this._error;
  }

  public get loading(): boolean {
    return this._loading;
  }

  public get currentOffset(): Number {
    return this._offset;
  }

  constructor(private readonly http: HttpClient) {
    const temp = StorageUtil.storageRead<Pokemon[]>(StorageKeys.PokeCache);

    if (temp !== undefined) {
      for (const pokemon of temp) {
        this._pokemonCache?.result?.push(pokemon);
      }
    }

    this.startPokemonPage();
  }

  public getPokemons(): void {
    this._loading = true;
    if (
      this._pokemonCache?.result &&
      this._pokemonCache.result.length > this._offset
    ) {
      for (let i = this._offset; i < this._offset + this._pageLimit; i++) {
        this._pokemons.push(this._pokemonCache.result[i]);
      }
      this._loading = false;
    } else {
      this.http
        .get<any>(
          `${apiCatalogue}?offset=${this._offset}&limit=${this._pageLimit}`
        )
        .pipe(
          finalize(() => {
            StorageUtil.storageSave(
              StorageKeys.PokeCache,
              this._pokemonCache?.result
            );
          })
        )
        .subscribe({
          next: (response) => {
            for (const pokemonUrl of response.results) {
              const pokemon = this.getPokemonData(
                pokemonUrl.url,
                pokemonUrl.name
              );
              this._pokemons.push(pokemon);
              this._pokemonCache?.result?.push(pokemon);
            }
            this._loading = false;
          },
        });
    }
  }

  public pokemonById(pokemonName: string): Pokemon | undefined {
    if (this._pokemonCache?.result) {
      const pokemon = this._pokemonCache?.result.find(
        (pokemon: Pokemon) => pokemon.name === pokemonName
      );
      if (pokemon) {
        return pokemon;
      }
    }
    return undefined;
  }

  public startPokemonPage(): void {
    this._offset = 0;
    this._pokemons = [];
    this.getPokemons();
  }

  public nextPokemonPage(): void {
    this._offset += this._pageLimit;
    this._pokemons = [];
    this.getPokemons();
  }

  public previousPokemonPage(): void {
    if (this._offset > 0) {
      this._offset -= this._pageLimit;
      this._pokemons = [];
      this.getPokemons();
    }
  }

  private getPokemonData(url: string, name: string): any {
    const pokeId = url.split('/').filter(Boolean).pop();
    if (pokeId !== undefined) {
      const id = +pokeId;
      return {
        id,
        name,
        url,
        image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
      };
    }
  }
}
