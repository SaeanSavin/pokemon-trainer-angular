import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { UserService } from './user.service';

const { apiKey, apiUsers } = environment;

@Injectable({
  providedIn: 'root',
})
export class CollectionService {
  private _loading: Boolean = false;

  public get loading(): Boolean {
    return this._loading;
  }

  constructor(
    private readonly http: HttpClient,
    private readonly userService: UserService
  ) {}

  public addToCollection(pokemon: Pokemon): Observable<User> {
    if (!pokemon) {
      throw new Error('addToCollection: No pokemon with name: ' + pokemon);
    }

    if (this.userService.inColletion(pokemon.name)) {
      throw new Error('addToCollection: Pokemon already in collection');
    }

    if (!this.userService.user) {
      throw new Error('addToCollecion: There is no user!');
    }

    const user: User = this.userService.user;

    return this.updateCollection([...user.pokemon, pokemon], user);
  }

  public removeFromCollection(pokemon: Pokemon): Observable<User> {
    if (!pokemon) {
      throw new Error('removeFromCollection: No pokemon with name: ' + pokemon);
    }

    if (!this.userService.inColletion(pokemon.name)) {
      throw new Error('removeFromCollection: Pokemon not in collection');
    }

    if (!this.userService.user) {
      throw new Error('removeFromCollection: There is no user!');
    }

    const user: User = this.userService.user;

    const index = user.pokemon.map((p) => p.name).indexOf(pokemon.name);

    if (index > -1) {
      user.pokemon.splice(index, 1);
    }

    return this.updateCollection(user.pokemon, user);
  }

  private updateCollection(pokemon: Pokemon[], user: User): Observable<User> {
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    this._loading = true;

    return this.http
      .patch<User>(`${apiUsers}/${user.id}`, { pokemon }, { headers })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        }),
        finalize(() => {
          this._loading = false;
        })
      );
  }
}
