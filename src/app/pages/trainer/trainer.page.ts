import { Component } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage {
  public get user(): User | undefined {
    return this.userService.user;
  }

  public get pokemonCollection(): Pokemon[] {
    if (this.userService.user?.pokemon) {
      return this.userService.user?.pokemon;
    }
    return [];
  }

  public onLogoutClick(): void {
    this.userService.user = undefined;
    this.router.navigateByUrl('/');
  }

  constructor(
    private readonly userService: UserService,
    private readonly router: Router
  ) {}
}
