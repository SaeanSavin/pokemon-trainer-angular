import { Component } from '@angular/core';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css'],
})
export class PaginatorComponent {
  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) {}

  public onChangeNext(): void {
    this.pokemonCatalogueService.nextPokemonPage();
  }
  public onChangePrevious(): void {
    this.pokemonCatalogueService.previousPokemonPage();
  }

  public isOnFirstPage(): Boolean {
    return this.pokemonCatalogueService.currentOffset === 0;
  }
}
