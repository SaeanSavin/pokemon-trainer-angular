import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-landing-form',
  templateUrl: './landing-form.component.html',
  styleUrls: ['./landing-form.component.css'],
})
export class LandingFormComponent {
  @Output() login: EventEmitter<void> = new EventEmitter();

  public loading: boolean = false;

  public error: string = '';

  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService
  ) {}

  public loginSubmit(landingForm: NgForm) {
    this.loading = true;
    const { username } = landingForm.value;

    console.log(username);

    this.loginService.login(username).subscribe({
      next: (user: User) => {
        this.loading = false;
        this.userService.user = user;
        this.login.emit();
      },
      error: (error) => {
        this.loading = false;
        this.error = error.message;
        console.log(error.message);
      },
    });
  }
}
