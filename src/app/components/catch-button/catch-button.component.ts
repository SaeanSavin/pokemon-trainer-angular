import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { CollectionService } from 'src/app/services/collection.service';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css'],
})
export class CatchButtonComponent {
  @Input() pokemonName: string = '';

  constructor(
    private readonly collectionService: CollectionService,
    private readonly catalogueService: PokemonCatalogueService,
    private readonly userService: UserService
  ) {}

  public inUserCollection(pokemon: string): boolean {
    return this.userService.inColletion(pokemon);
  }

  public onCatchClick(): void {
    const pokemon = this.catalogueService.pokemonById(this.pokemonName);

    if (pokemon) {
      this.collectionService.addToCollection(pokemon).subscribe({
        next: (user: User) => {
          console.log(
            'Successfully added ' + pokemon.name + ' to ' 
            + user.username + "'s collection"
          );
        },
        error: (error: HttpErrorResponse) => {
          console.log('ERROR', error.message);
        },
      });
    }
  }

  public onReleaseClick(): void {
    const pokemon = this.userService.user?.pokemon.find(
      (pokemon: Pokemon) => pokemon.name === this.pokemonName
    );

    if (pokemon) {
      this.collectionService.removeFromCollection(pokemon).subscribe({
        next: (user: User) => {
          console.log(
            'Successfully removed ' +
              pokemon.name + ' from ' 
              + user.username + "'s collection!"
          );
        },
        error: (error: HttpErrorResponse) => {
          console.log('ERROR', error.message);
        },
      });
    }
  }
}
