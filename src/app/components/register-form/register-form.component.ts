import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css'],
})
export class RegisterFormComponent {
  public loading: boolean = false;

  public error: string = '';

  constructor(
    private readonly router: Router,
    private readonly registerService: RegisterService
  ) {}

  public registerSubmit(registerForm: NgForm): void {
    this.loading = true;
    const { username } = registerForm.value;

    this.registerService.register(username).subscribe({
      next: (user: User) => {
        console.log('Register', user);
        this.loading = false;
        this.router.navigateByUrl('/');
      },
      error: (error) => {
        this.loading = false;
        this.error = error.message;
        console.log(error);
      },
    });
  }
}
